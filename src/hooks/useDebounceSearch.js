import { useEffect, useMemo, useState } from 'react';
import debounce from 'lodash.debounce';

const useDebounceSearch = () => {
  const [query, setQuery] = useState('');

  const handleQueryChange = (value) => setQuery(value);

  const debouncedChangeHandler = useMemo(
    () => debounce(handleQueryChange, 750),
    [query]);

  useEffect(() => {
    return () => {
      debouncedChangeHandler.cancel();
    };
  }, []);

  return [query, debouncedChangeHandler];
};

export default useDebounceSearch;
