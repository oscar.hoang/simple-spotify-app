import { useState, useEffect } from 'react';
import { search } from '../components/spotify';

const useArtistSearch = (query) => {
  const [artists, setArtists] = useState([]);
  const [offset, setOffset] = useState(0);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);
  const [currentQuery, setCurrentQuery] = useState('');

  const handleQueryOffset = (number) => {
    setOffset(currentQuery === query ? number : 0);
  };

  const next = () => {
    const subs = artists?.next?.split('&');
    const value = subs[2]?.split('=')[1] || '';
    handleQueryOffset(value);
  };

  const prev = () => {
    const subs = artists?.previous?.split('&');
    const value = subs[2]?.split('=')[1] || '';
    handleQueryOffset(value);
  };

  useEffect(() => {
    if (query !== '') {
      const runEffect = async () => {
        try {
          setIsLoading(true);
          const results = await search(query, offset);
          setCurrentQuery(query);
          setArtists(results.data.artists);
        } catch (err) {
          if (err?.response?.status === 401) {
            localStorage.removeItem('token');
          }
          setError(err?.response?.data?.error);
        }
        setIsLoading(false);
      };
      runEffect();
    }
  }, [query, offset]);

  return [artists, isLoading, error, next, prev];
};

export default useArtistSearch;
