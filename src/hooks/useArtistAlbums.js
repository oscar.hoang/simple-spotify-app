import { useState, useEffect } from 'react';
import { getAlbums } from '../components/spotify';

const useArtistAlbums = (id) => {
  const [albums, setAlbums] = useState([]);
  const [offset, setOffset] = useState(0);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);

  const handleQueryOffset = (number) => {
    setOffset(number);
  };

  const next = () => {
    const subs = albums?.next?.split('&');
    const value = subs[0]?.split('=')[1] || '';
    handleQueryOffset(value);
  };

  const prev = () => {
    const subs = albums?.previous?.split('&');
    const value = subs[0]?.split('=')[1] || '';
    handleQueryOffset(value);
  };

  useEffect(() => {
    if (id !== '') {
      const runEffect = async () => {
        try {
          setIsLoading(true);
          const results = await getAlbums(id, offset);
          setAlbums(results.data);
        } catch (err) {
          setError(err?.response?.data?.error);
        }
        setIsLoading(false);
      };
      runEffect();
    }
  }, [id, offset]);

  return [albums, isLoading, error, next, prev];
};

export default useArtistAlbums;
