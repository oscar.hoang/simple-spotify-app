import axios from 'axios';

// Improve Security and Hide keys in code
// Create .env file in root directory and add variables (below)
// ie. AUTH_ENDPOINT = 'https://accounts.spotify.com/authorize'
const AUTH_ENDPOINT = process.env.REACT_APP_SPOTIFY_AUTH_END_POINT;
const CLIENT_ID = process.env.REACT_APP_SPOTIFY_CLIENT_ID;
const URI = process.env.REACT_APP_SPOTIFY_REDIRECT_URI;

export const urlAuth = `${AUTH_ENDPOINT}?client_id=${CLIENT_ID}&redirect_uri=${URI}&response_type=token`;

export const search = (query, offset = 0) => {
  const token = JSON.parse(localStorage.getItem('token'));
  const res = axios.get(
    `https://api.spotify.com/v1/search?q=${query}&type=artist&offset=${offset}&limit=20`,
    {
      headers: {
        Authorization: `Bearer ${token}`,
        Accept: 'application/json',
      },
    }
  );
  return res;
};

export const getAlbums = (artistId, offset = 0) => {
  const token = JSON.parse(localStorage.getItem('token'));
  const res = axios.get(`https://api.spotify.com/v1/artists/${artistId}/albums?offset=${offset}&limit=20`,
    {
      headers: {
        Authorization: `Bearer ${token}`,
        Accept: 'application/json',
      },
    }
  );
  return res;
};
