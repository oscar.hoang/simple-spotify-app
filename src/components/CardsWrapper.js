import React from 'react';

const CardWrapper = ({ children }) => (
  <div
    className={`
      grid xs:grid-cols-1 sm:grid-cols-2
      md:grid-cols-3 lg:grid-cols-4 gap-10
      items-center justify-center
    `}
  >
    {children}
  </div>
);

export default CardWrapper;
