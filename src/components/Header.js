import React from 'react';

const Header = () => (
  <div className='flex item-center justify-center pt-6 pb-6 mb-6 border-b'>
    <div className='w-48'>
      <img src='/spotify-logo-rgb-black.png' width='200' height='66' />
    </div>
  </div>
);

export default Header;
