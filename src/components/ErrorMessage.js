import React from 'react';
import PropTypes from 'prop-types';

const ErrorMessage = ({ message }) => (
  <div role='alert' className='flex items-center mt-4 px-4 py-3 bg-red-400 rounded-full text-white text-sm font-bold'>
    <p>{message}</p>
  </div>
);

ErrorMessage.propTypes = {
  message: PropTypes.string
};

export default ErrorMessage;
