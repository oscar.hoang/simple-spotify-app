import React from 'react';
import PropTypes from 'prop-types';

const PageHeader = ({ data, next, prev, title, subtitle }) => {
  const prevDisabled = !data.previous;
  const nextDisabled = !data.next || data.offset > data.total;
  const iconSize = '24';

  return (
    <div className='flex items-center justify-between mb-4'>
      <div>
        {title ? <div className='text-3xl'>{title}</div> : null}
        <div className='text-xl'>{subtitle}</div>
      </div>
      <div className='flex'>
        <button
          disabled={prevDisabled}
          onClick={() => prev()}
          className={`
            bg-grey-light hover:bg-grey p-2 inline-flex items-center
            ${prevDisabled && 'cursor-not-allowed opacity-10'}
          `}>
          <img src='/chevron-left.svg' width={iconSize} height={iconSize}/>
        </button>
        <button
          disabled={nextDisabled}
          onClick={() => next()}
          className={`
            bg-grey-light hover:bg-grey p-2 inline-flex items-center
            ${nextDisabled && 'cursor-not-allowed opacity-10'}
          `}>
          <img src='/chevron-right.svg' width={iconSize} height={iconSize}/>
        </button>
      </div>
    </div>
  );
};

PageHeader.propTypes = {
  data: PropTypes.object,
  next: PropTypes.func,
  prev: PropTypes.func,
  title: PropTypes.string,
  subtitle: PropTypes.string
};

export default PageHeader;
