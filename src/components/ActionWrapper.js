import React from 'react';

const ActionWrapper = ({ children }) => (
  <div className='grid grid-cols-6 gap-4'>
    <div className='relative col-start-2 col-span-4 lg:col-start-3 lg:col-span-2'>
      <div className='my-12'>
        {children}
      </div>
    </div>
  </div>
);

export default ActionWrapper;
