import React from 'react';
import { Redirect } from 'react-router-dom';
import ActionWrapper from '../ActionWrapper';
import { urlAuth } from '../spotify';

const Login = () => {
  const [isLoading, setIsLoading] = React.useState(false);

  const buttonStyle = `
    flex flex-row flex-grow items-center justify-between
    relative p-4 bg-white hover:bg-gray-600
    text-black hover:text-white border-black
    border-2 rounded-full font-medium
    transition duration-300 ease-in-out
    ${isLoading ? 'cursor-not-allowed' : ''}
  `;

  const handleClick = () => {
    setIsLoading(true);
    window.location = urlAuth;
  };

  const token = JSON.parse(localStorage.getItem('token'));
  if (token) return <Redirect to='/artist' />;

  return (
    <div className='container mx-auto'>
      <ActionWrapper>
        <button
          disabled={isLoading}
          className={buttonStyle}
          style={{ width: '100%' }}
          onClick={() => handleClick()}>
          <span>{!isLoading ? 'Login with Spotify' : 'Loading...'}</span>
          <img
            className='absolute -right-1.5'
            src='/spotify.svg' width='80' height='80'/>
        </button>
      </ActionWrapper>
    </div>
  );
};

export default Login;
