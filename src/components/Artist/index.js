import React from 'react';
import { Redirect, useHistory } from 'react-router-dom';

import useDebounceSearch from '../../hooks/useDebounceSearch';
import useArtistSearch from '../../hooks/useArtistSearch';

import Search from '../Search';
import ErrorMessage from '../ErrorMessage';
import PageHeader from '../PageHeader';
import CardsWrapper from '../CardsWrapper';
import ActionWrapper from '../ActionWrapper';
import ArtistList from './ArtistList';

const Artist = () => {
  const history = useHistory();
  const [query, debouncedChangeHandler] = useDebounceSearch();
  const [artists, isLoading, error, next, prev] = useArtistSearch(query);

  React.useEffect(() => {
    if (artists?.items?.length) { // List of Artists present
      history.push({
        pathname: '/artist',
        search: `?q=${query}`
      });
    }
  }, [artists]);

  React.useEffect(() => {
    if (history?.location?.search) {
      const arr = history.location.search.split('=');
      debouncedChangeHandler(arr[1]);
    }
  }, []);

  const handleSubmit = (e) => e.preventDefault();

  const token = JSON.parse(localStorage.getItem('token'));
  if (!token) return <Redirect to='/' />;

  return (
    <div className='container mx-auto'>
      <ActionWrapper>
        <Search
          handleChange={(e) => debouncedChangeHandler(e.target.value)}
          handleSubmit={handleSubmit}
        />
        {isLoading && (
          <div className='absolute bottom-0 left-1/3 ml-12'>
            <div className='font-bold text-center'>Loading...</div>
          </div>
        )}
      </ActionWrapper>

      {error && (<ActionWrapper><ErrorMessage message={error.message}/></ActionWrapper>)}

      {query !== '' && artists?.items?.length === 0 && !isLoading && !error && (
        <div className='flex flex-col items-center justify-center py-4'>
          <div className='mb-2'>
            <div className='font-bold text-2xl'>No results found for &quot;{query}&quot;</div>
          </div>
          <div className='text-base'>
            Please make sure your words are spelled correctly or use less or different keywords.
          </div>
        </div>
      )}

      {artists?.items?.length > 0 && <PageHeader data={artists} next={next} prev={prev} subtitle='Artists' />}

      <CardsWrapper>
        <ArtistList artists={artists.items} />
      </CardsWrapper>
    </div>
  );
};

export default Artist;
