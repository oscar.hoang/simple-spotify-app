import React from 'react';
import PropTypes from 'prop-types';
import ArtistListItem from './ArtistListItem';

const ArtistList = ({ artists }) => (artists || []).map((artist) => (
  <ArtistListItem key={artist.id} artist={artist} />
));

ArtistList.propTypes = {
  artists: PropTypes.array
};

export default ArtistList;
