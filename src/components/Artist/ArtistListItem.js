import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import StarRating from '../StarRating';
import CardImage from '../CardImage';

const ArtistListItem = ({ artist }) => (
  <div className='grid col-span-1'>
    <Link
      className='rounded overflow-hidden shadow-lg'
      to={{
        pathname: `/artist/${artist.id}/albums`,
        state: { artistName: artist.name }
      }}>
      <CardImage data={artist} />
      <div className='px-2 py-4'>
        <div className='mb-4'>
          <div className='font-medium text-lg'>{((artist.name).substring(0, 20))}</div>
          <div className='text-sm'>{artist.followers.total} followers</div>
        </div>
        <StarRating popularity={artist.popularity} />
      </div>
    </Link>
  </div>
);

ArtistListItem.propTypes = {
  artist: PropTypes.object
};

export default ArtistListItem;
