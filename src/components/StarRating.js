import React from 'react';
import PropTypes from 'prop-types';

const StarRating = ({ popularity }) => {
  const value = Math.round(((popularity / 100) * 5));
  const activeColor = '#F8E71C';
  const inactiveColor = '#E9E9E9';
  const size = '24px';

  const stars = Array.from({ length: 5 }, () => '★');

  return (
    <div>
      {stars.map((star, index) => {
        let style = inactiveColor;
        if (index < value) {
          style = activeColor;
        }
        return (
          <span key={index} style={{
            color: style,
            width: size,
            height: size
          }}>
            {star}
          </span>
        );
      })}
    </div>
  );
};

StarRating.propTypes = {
  popularity: PropTypes.number
};

export default StarRating;
