import React from 'react';
import PropTypes from 'prop-types';

const CardImage = ({ data }) => (
  <div
    className='flex items-center justify-center bg-gray-800'
    style={{ height: '226px' }}
  >
    {data?.images[0]?.url ? (
      <img
        className='object-cover h-full w-full'
        src={data.images[0].url}
        style={{ width: '100%' }}
      />
    ) : (
      <div className='text-white'>Image is unavailable</div>
    )}
  </div>
);

CardImage.propTypes = {
  data: PropTypes.object
};

export default CardImage;
