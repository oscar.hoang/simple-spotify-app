import React from 'react';
import PropTypes from 'prop-types';
import SearchIcon from './searchIcon';

const Search = ({ value, handleSubmit, handleChange }) => {
  return (
    <form onSubmit={handleSubmit}>
      <div className='relative text-gray-600 focus-within:text-gray-400'>
        <SearchIcon />
        <input
          type='search'
          name='q'
          className={`
            min-w-full py-2 pl-10 pr-2 text-sm text-black border-2
            rounded-full focus:outline-nonefocus:text-gray-900
            w-full
          `}
          placeholder='Search for an artist...'
          autoComplete='off'
          value={value}
          onChange={(e) => handleChange(e)}
        />
      </div>
    </form>
  );
};

Search.propTypes = {
  value: PropTypes.string,
  handleChange: PropTypes.func,
  handleSubmit: PropTypes.func
};

export default Search;
