import React from 'react';
import PropTypes from 'prop-types';
import CardImage from '../CardImage';

const AlbumListItem = ({ album }) => {
  const renderArtistNames = () => {
    return album.artists.map((artist, index) => {
      if (index < album.artists.length - 1) {
        return <span key={artist.id}>{artist.name},&nbsp;</span>;
      }
      return <span key={artist.id}>{artist.name}</span>;
    });
  };

  return (
    <div className='grid col-span-1'>
      <div className='rounded overflow-hidden shadow-lg'>
        <CardImage data={album} />
        <div className='px-2 py-4'>
          <div className='mb-4'>
            <div className='font-medium text-lg'>{((album.name).substring(0, 12))}</div>
            <div className='text-sm'>{renderArtistNames()}</div>
          </div>
          <div>{album.release_date}</div>
          <div>{album.total_tracks} Tracks</div>
        </div>
        <div className='bg-green-500'>
          <a
            target='_blank'
            href={`${album.external_urls.spotify}`}
            className='flex items-center justify-center p-2 text-white'
            rel='noreferrer'
          >
            Preview on Spotify
          </a>
        </div>
      </div>
    </div>
  );
};

AlbumListItem.propTypes = {
  album: PropTypes.object
};

export default AlbumListItem;
