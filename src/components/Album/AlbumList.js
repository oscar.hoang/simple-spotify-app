import React from 'react';
import PropTypes from 'prop-types';
import AlbumListItem from './AlbumListItem';

const AlbumList = ({ albums }) => (albums || []).map((album) => (
  <AlbumListItem key={album.id} album={album} />
));

AlbumList.propTypes = {
  albums: PropTypes.array
};

export default AlbumList;
