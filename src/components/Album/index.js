import React from 'react';
import { useLocation, useParams } from 'react-router-dom';
import useArtistAlbums from '../../hooks/useArtistAlbums';
import ErrorMessage from '../ErrorMessage';
import CardsWrapper from '../CardsWrapper';
import PageHeader from '../PageHeader';
import AlbumList from './AlbumList';

const Albums = () => {
  const { artistId } = useParams();
  const { state: { artistName = '' } = {} } = useLocation();
  const [albums, isLoading, error, next, prev] = useArtistAlbums(artistId);

  return (
    <div className='container mx-auto'>
      {albums?.items?.length > 0 && (
        <PageHeader data={albums} next={next} prev={prev} title={artistName} subtitle='albums' />
      )}

      {error && (<div className='mb-4'><ErrorMessage message={error.message}/></div>)}

      {isLoading && (<div className='flex items-center justify-center'>Loading...</div>)}

      <CardsWrapper>
        <AlbumList albums={albums.items} />
      </CardsWrapper>
    </div>
  );
};

export default Albums;
