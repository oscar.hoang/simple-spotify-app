import React from 'react';
import PropTypes from 'prop-types';
import { Route, Redirect, useHistory, useLocation } from 'react-router-dom';

const PrivateRoute = ({ component: Component, ...rest }) => {
  const location = useLocation();
  const history = useHistory();

  const substring = location.hash.substring(1);

  if (substring) {
    const arr = substring.split('&');
    const accessToken = arr[0]?.split('=')[1] || '';

    localStorage.setItem('token', JSON.stringify(accessToken));

    history.push({});
  }

  const token = JSON.parse(localStorage.getItem('token'));

  if (!token) {
    return <Redirect to='/' />;
  }
  return <Route {...rest} render={(props) => (<Component {...props} />)}/>;
};

PrivateRoute.propTypes = {
  component: PropTypes.elementType
};

export default PrivateRoute;
