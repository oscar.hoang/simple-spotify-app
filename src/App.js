import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Link
} from 'react-router-dom';
import Album from './components/Album';
import Artist from './components/Artist';
import Login from './components/Login';
import PrivateRoute from './components/PrivateRoute';
import Header from './components/Header';
import ActionWrapper from './components/ActionWrapper';

const NotFound = () => (
  <div className='container mx-auto'>
    <ActionWrapper>
      <div className='mb-4'>
        <div className='text-3xl text-center'>404 - Not Found!</div>
      </div>
      <Link to="/" className='bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded'>Go Back</Link>
    </ActionWrapper>
  </div>
);

const CALLBACK_PATH = '/artist';

const App = () => {
  return (
    <Router>
      <Header/>
      <Switch>
        <Route path="/" exact>
          <Login />
        </Route>
        <PrivateRoute path={CALLBACK_PATH} exact>
          <Artist />
        </PrivateRoute>
        <PrivateRoute path={`${CALLBACK_PATH}/:artistId/albums`} exact>
          <Album />
        </PrivateRoute>
        <Route component={NotFound} />
      </Switch>
    </Router>
  );
};

export default App;
